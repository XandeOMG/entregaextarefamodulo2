import java.util.Scanner;
public class Ex1 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        int opcao;

        do{
            System.out.println("---- Menu de opções ----");

            System.out.println("1. Opção 1");        
            System.out.println("2. Opção 2");
            System.out.println("3. Sair");

            System.out.print("Digite a opção desejada: ");
            opcao = scanner.nextInt();
            System.out.println(" ");
            
            if(opcao>3){
                System.out.println("Opção inexistente");
                System.out.println(" ");
            }else{
                switch(opcao){

                    case 1:
                        System.out.println("Você escolheu a primeira opção.");
                        System.out.println(" ");
                        break;
                    
                    case 2:
                        System.out.println("Você escolheu a segunda opção.");
                        System.out.println(" ");
                        break;

                    case 3:
                        System.out.println("Seu programa foi encerrado.");
                        System.out.println(" ");
                        break;
                }
            }
        }while(opcao!=3);

        scanner.close();
    }
}
